<?php

namespace ParticipatoryArchives;

if (!class_exists(\Generic\AbstractModule::class)) {
    require file_exists(dirname(__DIR__) . '/Generic/AbstractModule.php')
        ? dirname(__DIR__) . '/Generic/AbstractModule.php'
        : __DIR__ . '/src/Generic/AbstractModule.php';
}

use Generic\AbstractModule;
use Laminas\Form\Element;
use Laminas\Mvc\Controller\AbstractController;
use Laminas\View\Renderer\PhpRenderer;
// use Omeka\Module\AbstractModule;
// use ParticipatoryArchives\Form\ConfigForm;

class Module extends AbstractModule
{
    const NAMESPACE = __NAMESPACE__;
    // TODO: automate resource template creation
    /* const resource_templates = [
        'Collection', 'Call', 'Reaction'
    ]; */

    /** Module body **/

    /**
     * Get this module's configuration array.
     *
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get this module's configuration form.
     *
     * @param PhpRenderer $view
     * @return string
     */
    public function getConfigForm(PhpRenderer $renderer)
    {
        $settings = $this->getServiceLocator()->get('Omeka\Settings');
        $formManager = $this->getServiceLocator()->get('FormElementManager');
        $form = $formManager->get('ParticipatoryArchives\Form\ConfigForm');

        // TODO: refactor into ConfigForm, but somehow can't set or access globalSettings there
        $form
            ->add([
                'name' => 'auto_add_new_collections_to_default_site',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Add new collections to default site',
                    'info' => 'Defines if user generated collections are automatically added to the ressources of the default site.',
                ],
            ])
            ->add([
                'name' => 'anonymous_key_identity',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'anonymous_key_identity',
                    'label' => 'Anonymous key identity string',
                ],
                'attributes' => [
                    'value' => $settings->get('anonymous_key_identity'),
                ]
            ])
            ->add([
                'name' => 'anonymous_key_credential',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'anonymous_key_credential',
                    'label' => 'Anonymous key credential string',
                ],
                'attributes' => [
                    'value' => $settings->get('anonymous_key_credential'),
                ]
            ])
            ->add([
                'name' => 'rt_id_collection',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'rt_id_collection',
                    'label' => 'Resource Template ID: Collection',
                ],
                'attributes' => [
                    'value' => $settings->get('rt_id_collection'),
                ]
            ])
            ->add([
                'name' => 'rt_id_call',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'rt_id_call',
                    'label' => 'Resource Template ID: Call',
                ],
                'attributes' => [
                    'value' => $settings->get('rt_id_call'),
                ]
            ])
            ->add([
                'name' => 'rt_id_reaction',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'rt_id_reaction',
                    'label' => 'Resource Template ID: Reaction',
                ],
                'attributes' => [
                    'value' => $settings->get('rt_id_reaction'),
                ]
            ]);

        return $renderer->formCollection($form);
    }

    /**
     * Handle this module's configuration form.
     *
     * @param AbstractController $controller
     * @return bool False if there was an error during handling
     */
    public function handleConfigForm(AbstractController $controller)
    {
        $params = $controller->params()->fromPost();

        $settings = $this->getServiceLocator()->get('Omeka\Settings');
        $settings->set('anonymous_key_identity', $params['anonymous_key_identity']);
        $settings->set('anonymous_key_credential', $params['anonymous_key_credential']);
        $settings->set('rt_id_collection', $params['rt_id_collection']);
        $settings->set('rt_id_call', $params['rt_id_call']);
        $settings->set('rt_id_reaction', $params['rt_id_reaction']);

        return true;
    }

    protected function postInstall(): void
    {
        $api        = $this->getServiceLocator()->get('Omeka\ApiManager');
        $settings   = $this->getServiceLocator()->get('Omeka\Settings');

        $resource_templates = $api->search('resource_templates')->getContent();

        $rt_collection_exists = false;
        $rt_call_exists = false;
        $rt_reaction_exists = false;

        foreach ($resource_templates as $key => $resource_template) {
            if($resource_template->label() == 'Collection') {
                $rt_collection_exists = true;
                $rt_collection = $resource_template;
            }
            if($resource_template->label() == 'Call') {
                $rt_call_exists = true;
                $rt_call = $resource_template;
            }
            if($resource_template->label() == 'Reaction') {
                $rt_reaction_exists = true;
                $rt_reaction = $resource_template;
            }
        }

        // TODO: extend resource template creation
        if(!$rt_collection_exists) {
            $data = [
                'o:label' => 'Collection',
                '@type' => 'o:ResourceTemplate'
            ];

            $rt_collection = $api->create('resource_templates', $data);
        }

        if(!$rt_call_exists) {
            $data = [
                'o:label' => 'Call',
                '@type' => 'o:ResourceTemplate'
            ];

            $rt_call = $api->create('resource_templates', $data);
        }

        if(!$rt_reaction_exists) {
            $data = [
                'o:label' => 'Reaction',
                '@type' => 'o:ResourceTemplate'
            ];

            $rt_reaction = $api->create('resource_templates', $data);
        }

        $settings->set('rt_id_collection', $rt_collection->id());
        $settings->set('rt_id_call', $rt_call->id());
        $settings->set('rt_id_reaction', $rt_reaction->id());

    }
}

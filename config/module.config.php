<?php

namespace ParticipatoryArchives;

return [
    'view_manager' => [
        'template_path_stack' => [
            dirname(__DIR__) . '/view',
        ],
    ],

    'controllers' => [
        'invokables' => [
            'ParticipatoryArchives\Controller\Site\Curation' => Controller\Site\CurationController::class,
            'ParticipatoryArchives\Controller\Site\Calls' => Controller\Site\CallsController::class,
            'ParticipatoryArchives\Controller\Site\Reactions' => Controller\Site\ReactionsController::class,
            'ParticipatoryArchives\Controller\Site\Explore' => Controller\Site\ExploreController::class,
            'ParticipatoryArchives\Controller\Site\Maintenance' => Controller\Site\MaintenanceController::class,
            'ParticipatoryArchives\Controller\Site\Contexts' => Controller\Site\ContextsController::class,
        ],
        /* 'factories' => [
            'ParticipatoryArchives\Controller\Site\Reactions' => ParticipatoryArchives\Service\Controller\Site\ReactionsControllerFactory::class,
        ], */
    ],

    'form_elements' => [
        'invokables' => [
            Form\ConfigForm::class => Form\ConfigForm::class,
        ],
    ],
    
    'participatoryarchives' => [
        'config' => [
            'anonymous_key_identity' => '',
            'anonymous_key_credential' => '',
            'rt_id_collection' => '',
            'rt_id_call' => '',
            'rt_id_reaction' => ''
        ],
    ],

    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'pia-curation' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/curation',
                            'defaults' => [
                                '__NAMESPACE__' => 'ParticipatoryArchives\Controller\Site',
                                'controller' => Controller\Site\CurationController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'default' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/:action',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'pia-maintenance' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/maintenance',
                            'defaults' => [
                                '__NAMESPACE__' => 'ParticipatoryArchives\Controller\Site',
                                'controller' => Controller\Site\MaintenanceController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'default' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/:action',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'pia-calls' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/calls',
                            'defaults' => [
                                '__NAMESPACE__' => 'ParticipatoryArchives\Controller\Site',
                                'controller' => Controller\Site\CallsController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'default' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/:action[/:id]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '\d+',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'pia-reactions' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/reactions',
                            'defaults' => [
                                '__NAMESPACE__' => 'ParticipatoryArchives\Controller\Site',
                                'controller' => Controller\Site\ReactionsController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'default' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/:action[/:id]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '\d+',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'pia-contexts' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/contexts',
                            'defaults' => [
                                '__NAMESPACE__' => 'ParticipatoryArchives\Controller\Site',
                                'controller' => Controller\Site\ContextsController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'default' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/:action[/:id]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '\d+',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'pia-explore' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/explore',
                            'defaults' => [
                                '__NAMESPACE__' => 'ParticipatoryArchives\Controller\Site',
                                'controller' => Controller\Site\ExploreController::class,
                                'action' => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'default' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/:action[/:id]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '\d+',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];

<?php

namespace ParticipatoryArchives\Controller\Site;

use CURLFile;
use Error;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ServiceManager\ServiceManager;
use Laminas\View\Model\ViewModel;

class ReactionsController extends AbstractActionController
{
    /* protected $services;

    public function __construct(ServiceManager $services)
    {
        $this->services = $services;
    } */

    /**
     */
    public function indexAction()
    {
    }

    /**
     */
    public function createAction()
    {
    }

    /**
     */
    public function storeAction()
    {
        $rt_id_reaction = $this->settings()->get('rt_id_reaction');

        $data = [
            'o:resource_class' => [
                '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/25',
                'o:id' => 25
            ],
            'o:resource_template' => [
                '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_templates/' . $rt_id_reaction,
                'o:id' => $rt_id_reaction
            ],
            'schema:name' => [
                [
                    'type' => 'literal',
                    'property_id' => 957,
                    '@value' => $this->params()->fromPost('name')
                ]
            ],
            'schema:license' => [
                [
                    'type' => 'uri',
                    'property_id' => 876,
                    '@id' => 'https://creativecommons.org/licenses/by/4.0/legalcode'
                ]
            ],
        ];

        if ($this->params()->fromPost('item_id') != '') {
            $item_id = $this->params()->fromPost('item_id');

            $data['schema:isRelatedTo'] = [
                [
                    'type' => 'resource:item',
                    'property_id' => 814,
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/' . $item_id,
                    'value_resource_id' => $item_id
                ]
            ];
        }

        if ($this->params()->fromPost('collection_id') != '') {
            $collection_id = $this->params()->fromPost('collection_id');

            $data['o:item_set'] = [
                [
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $collection_id,
                    'o:id' => $collection_id
                ]
            ];
        }

        if ($this->params()->fromPost('creator') != '') {
            $data['schema:creator'] = [
                [
                    'type' => 'literal',
                    'property_id' => 1511,
                    '@value' => $this->params()->fromPost('creator')
                ]
            ];
        }

        if ($this->params()->fromPost('comment') != '') {
            $data['schema:comment'] = [
                [
                    'type' => 'html',
                    'property_id' => 417,
                    '@value' => $this->params()->fromPost('comment')
                ]
            ];
        }

        if ($this->params()->fromPost('answers') != '') {
            $answers = [];

            for ($i = 0; $i < count($this->params()->fromPost('answers')); $i++) {
                $answer = $this->params()->fromPost('answers')[$i];
                $question = $this->params()->fromPost('questions')[$i];

                $answers[] = [
                    'type' => 'literal',
                    'property_id' => 1587,
                    '@value' => $answer,
                    '@annotation' => [
                        'schema:question' => [
                            [
                                'type' => 'literal',
                                'property_id' => 1151,
                                '@value' => $question,
                            ]
                        ]
                    ]
                ];
            }
            foreach ($this->params()->fromPost('answers') as $key => $answer) {
            }

            $data['schema:suggestedAnswer'] = $answers;
        }

        if ($this->params()->fromPost('date') != '') {
            $data['schema:temporal'] = [
                [
                    'type' => 'numeric:timestamp',
                    'property_id' => 1363,
                    '@value' => $this->params()->fromPost('date')
                ]
            ];
        }

        if ($this->params()->fromPost('coordinates') != '') {
            $data['schema:geo'] = [
                [
                    'type' => 'geometry:geography:coordinates',
                    'property_id' => 681,
                    '@value' => $this->params()->fromPost('coordinates')
                ]
            ];
        }

        $files_count = count($_FILES['file']['name']);

        if ($files_count > 0 && $_FILES['file']['name'][0] != '') {
            $data['o:media'] = [];

            for ($i = 0; $i < $files_count; $i++) {
                $data['o:media'][] = [
                    'o:ingester' => 'upload',
                    'file_index' => strval($i)
                ];
            }
        }

        if (!$this->identity()) {
            $curl = curl_init();

            $api_url = 'https://' . $_SERVER['HTTP_HOST'] . '/api/items?key_identity=' .
                $this->settings()->get('anonymous_key_identity') . '&key_credential=' . $this->settings()->get('anonymous_key_credential');

            $post_data = [
                'data' => json_encode($data)
            ];

            $files_count = count($_FILES['file']['name']);

            if ($files_count > 0 && $_FILES['file']['name'][0] != '') {
                for ($i = 0; $i < $files_count; $i++) {
                    $post_data['file[' . $i . ']'] = new CURLFile(
                        $_FILES['file']['tmp_name'][$i]
                    );
                }
            }

            curl_setopt_array($curl, array(
                CURLOPT_URL => $api_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POST => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $post_data,
            ));

            $response = curl_exec($curl);

            curl_close($curl);
        } else {
            $reaction = $this->api()->create('items', $data, $this->params()->fromFiles())->getContent();
        }

        if ($this->params()->fromPost('redirect_id')) {
            $redirect_item = $this->api()->read('items', $this->params()->fromPost('redirect_id'))->getContent();

            if ($redirect_item->resourceTemplate()->id() == 10) {
                return $this->redirect()->toUrl('/s/explore/calls/show/' . $redirect_item->id());
            } else {
                return $this->redirect()->toUrl('/s/explore/item/' . $redirect_item->id());
            }
        } else if ($this->params()->fromPost('collection_id')) {
            return $this->redirect()->toUrl('/s/explore/item-set/' . $this->params()->fromPost('collection_id'));
        }
    }

    /**
     */
    public function showAction()
    {
        $response = $this->api()->read('items', $this->params('id'));
        $item = $response->getContent();

        $view = new ViewModel;
        $view->setVariable('item', $item);

        return $view;
    }
}

<?php

namespace ParticipatoryArchives\Controller\Site;

use Error;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class MaintenanceController extends AbstractActionController
{


    /**
     * 
     */
    public function importCollectionAction()
    {
        if($this->params()->fromPost('collection')) {
            $params = explode('";"', trim(preg_replace('/\s\s+/', ' ', $this->params()->fromPost('collection')), '"'));

            $site_item_sets = [];

            $rt_id_collection = $this->settings()->get('rt_id_collection');

            $collection = $this->api()
                    ->create('item_sets', [
                        'schema:name' => [
                            [
                                'type' => 'literal',
                                'property_id' => 957,
                                '@value' => $params[0]
                            ]
                        ],
                        'schema:description' => [
                            [
                                'type' => 'literal',
                                'property_id' => 1621,
                                '@value' => $params[1]
                            ]
                        ],
                        'o:is_open' => true,
                        'o:resource_class' => [
                            '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/254',
                            'o:id' => 254
                        ],
                        'o:resource_template' => [
                            '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_templates/' . $rt_id_collection,
                            'o:id' => $rt_id_collection
                        ]
                    ])->getContent();
            
            $collection_id = $collection->id();

            $site = $this->api()->read('sites', 1)->getContent();
            $site_item_sets = [];

            foreach ($site->siteItemSets() as $key => $set) {
                $site_item_sets[] = [
                    'o:item_set' => [
                        '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/'.$set->itemSet()->id(),
                        'o:id' => $set->itemSet()->id()
                    ]
                ];
            }

            $site_item_sets[] = [
                    'o:item_set' => [
                        '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/'.$collection_id,
                        'o:id' => $collection_id
                    ]
                ];

            $this->api()
                ->update('sites', 1, ['o:site_item_set' => $site_item_sets], [], [
                    'isPartial' => true,
                    'collectionAction' => 'append' // remove
                ]);

            $data = [
                'o:item_set' => [
                    [
                        '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/'.$collection_id,
                        'o:id' => $collection_id
                    ]
                ]
            ];

            foreach (explode(',', $params[2]) as $key => $signature) {
                $item = $this->api()
                    ->searchOne('items', [
                        'fulltext_search' => $signature
                    ])->getContent();

                if($item){
                    $this->api()
                        ->update('items', $item->id(), $data, [], [
                            'isPartial' => true,
                            'collectionAction' => 'append'
                        ]);
                }
                
            }

            return $this->redirect()->toUrl('/s/explore/item-set/'.$collection_id);
        }
    }
}

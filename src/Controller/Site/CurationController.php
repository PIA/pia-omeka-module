<?php

namespace ParticipatoryArchives\Controller\Site;

use Error;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class CurationController extends AbstractActionController
{

    /**
     * This enables a visitor to add photos to a
     * new or already existing collection.
     * 
     * @param $_POST['collection_id']
     * @param $_POST['collection_label']
     * @param $_POST[{IDs}]
     */
    public function appendAction()
    {
        $params = $this->params()->fromPost();
        $collection_id = $this->params()->fromPost('collection_id');

        $site_item_sets = [];

        if ($this->params()->fromPost('collection_label') != '') {

            $rt_id_collection = $this->settings()->get('rt_id_collection');

            $collection = $this->api()
                ->create('item_sets', [
                    'schema:name' => [
                        [
                            'type' => 'literal',
                            'property_id' => 957,
                            '@value' => $this->params()->fromPost('collection_label')
                        ]
                    ],
                    'o:is_open' => true,
                    'o:resource_class' => [
                        '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/254',
                        'o:id' => 254
                    ],
                    'o:resource_template' => [
                        '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_templates/' . $rt_id_collection,
                        'o:id' => $rt_id_collection
                    ]
                ])->getContent();

            $collection_id = $collection->id();

            $site = $this->api()->read('sites', 1)->getContent();
            $site_item_sets = [];

            foreach ($site->siteItemSets() as $key => $set) {
                $site_item_sets[] = [
                    'o:item_set' => [
                        '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $set->itemSet()->id(),
                        'o:id' => $set->itemSet()->id()
                    ]
                ];
            }

            $site_item_sets[] = [
                'o:item_set' => [
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $collection_id,
                    'o:id' => $collection_id
                ]
            ];

            $this->api()
                ->update('sites', 1, ['o:site_item_set' => $site_item_sets], [], [
                    'isPartial' => true,
                    'collectionAction' => 'append' // remove
                ]);
        }

        if ($collection_id == '') {
            print('Either choose a collection or set a new label…');
            return;
        }

        $data = [
            'o:item_set' => [
                [
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $collection_id,
                    'o:id' => $collection_id
                ]
            ]
        ];

        foreach ($params as $key => $param) {
            if (!in_array($key, ['url', 'collection_id', 'collection_label'])) {
                $this->api()
                    ->update('items', $key, $data, [], [
                        'isPartial' => true,
                        'collectionAction' => 'append'
                    ]);
            }
        }

        return $this->redirect()->toUrl('/s/explore/item-set/' . $collection_id);
    }
    /**
     * Remove object from a collection.
     * 
     * @param $_POST['collection_id']
     * @param $_POST['item_id']
     */
    public function removeAction()
    {
        $collection_id = $this->params()->fromPost('collection_id');
        $item_id = $this->params()->fromPost('item_id');

        $data = [
            'o:item_set' => [
                [
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $collection_id,
                    'o:id' => $collection_id
                ]
            ]
        ];

        $this->api()
            ->update('items', $item_id, $data, [], [
                'isPartial' => true,
                'collectionAction' => 'remove'
            ]);

        return $this->redirect()->toUrl('/s/explore/item-set/' . $collection_id);
    }

    /**
     * This enables a visitor to add photos to a
     * new or already existing collection.
     * 
     * @param $_POST['child_id']
     * @param $_POST['parent_id']
     * @param $_POST[{IDs}]
     */
    public function appendCollectionAction()
    {
        $params = $this->params()->fromPost();

        $child_id = $this->params()->fromPost('child_id');
        $parent_id = $this->params()->fromPost('parent_id');

        $site_item_sets = [];

        if ($parent_id == '') {
            print('Chose a parent…');
            return;
        }

        if ($child_id == '') {
            print('No child given…');
            return;
        }

        // TODO: automatic map properties and property ids, avoid hardcoding them
        $data = [
            'schema:isPartOf' => [
                [
                    'type' => 'resource:itemset',
                    'property_id' => 1626,
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $parent_id,
                    'value_resource_id' => $parent_id
                ]
            ]
        ];

        $this->api()
            ->update('item_sets', $child_id, $data, [], [
                'isPartial' => true,
                'collectionAction' => 'append'
            ]);

        return $this->redirect()->toUrl('/s/explore/item-set/' . $parent_id);
    }
}

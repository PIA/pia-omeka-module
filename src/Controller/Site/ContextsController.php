<?php

namespace ParticipatoryArchives\Controller\Site;

use Error;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class ContextsController extends AbstractActionController
{

    /**
     */
    public function indexAction()
    {
    }

    /**
     */
    public function createAction()
    {
    }

    /**
     */
    public function storeAction()
    {
        $collection_id = $this->params()->fromPost('collection_id');

        $data = [
            'o:resource_class' => [
                '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/306',
                'o:id' => 306
            ],
            'o:resource_template' => [
                '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_templates/12',
                'o:id' => 12
            ],
            'schema:name' => [
                [
                    'type' => 'literal',
                    'property_id' => 957,
                    '@value' => $this->params()->fromPost('name')
                ]
            ],
            'dcterms:type' => [
                [
                    'type' => 'numeric:integer',
                    'property_id' => 8,
                    '@value' => $this->params()->fromPost('type')
                ]
            ],
            'o:item_set' => [
                [
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/'.$collection_id,
                    'o:id' => $collection_id
                ]
            ]
        ];

        if($this->params()->fromPost('description') != '') {
            $data['schema:description'] = [
                [
                    'type' => 'literal',
                    'property_id' => 1621,
                    '@value' => $this->params()->fromPost('description')
                ]
            ];
        }

        $context = $this->api()->create('items', $data)->getContent();

        return $this->redirect()->toUrl('show/' . $context->id());
    }

    /**
     */
    public function showAction()
    {
        $response = $this->api()->read('items', $this->params('id'));
        $context = $response->getContent();

        $view = new ViewModel;
        $view->setVariable('context', $context);

        return $view;
    }

    /**
     */
    public function editAction()
    {
        $response = $this->api()->read('items', $this->params('id'));
        $context = $response->getContent();

        $view = new ViewModel;
        $view->setVariable('context', $context);

        return $view;
    }

    /**
     */
    public function updateAction()
    {
        $context_id = $this->params()->fromPost('context_id');

        $data = [
            'schema:name' => [
                [
                    'type' => 'literal',
                    'property_id' => 957,
                    '@value' => $this->params()->fromPost('name')
                ]
            ],
            'schema:description' => [
                [
                    'type' => 'literal',
                    'property_id' => 1621,
                    '@value' => $this->params()->fromPost('description')
                ]
            ],
            'dcterms:type' => [
                [
                    'type' => 'numeric:integer',
                    'property_id' => 8,
                    '@value' => intval($this->params()->fromPost('type')),
                    '@type' => 'http://www.w3.org/2001/XMLSchema#integer'
                ]
            ],
            'schema:dataset' => [
                [
                    'type' => 'literal',
                    'property_id' => 1512,
                    '@value' => $this->params()->fromPost('dataset')
                ]
            ]
        ];

        $this->api()
            ->update('items', $context_id, $data, [], [
                'isPartial' => true,
                'collectionAction' => 'replace'
            ]);

        return $this->redirect()->toUrl('show/' . $context_id);
    }
}

<?php

namespace ParticipatoryArchives\Controller\Site;

use Error;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class ExploreController extends AbstractActionController
{

    /**
     * 
     */
    public function nearbyAction()
    {
        $view = new ViewModel;

        if (isset($this->params()->fromQuery('geo')['around']['longitude'])) {

            $radius = $this->params()->fromQuery('geo')['around']['radius'] ?? 100;
            $unit = $this->params()->fromQuery('geo')['around']['unit'] ?? 'm';

            $search = $this->api()
                ->search('items', [
                    'geo' => [
                        'around' => [
                            'latitude' => $this->params()->fromQuery('geo')['around']['latitude'],
                            'longitude' => $this->params()->fromQuery('geo')['around']['longitude'],
                            'radius' => $radius,
                            'unit' => $unit
                        ]
                    ]
                ]);

            $view->setVariable('items', $search->getContent());
        } elseif ($this->params()->fromQuery('item_set_id')) {
            $search = $this->api()
                ->search('items', [
                    'item_set_id' => $this->params()->fromQuery('item_set_id')
                ]);

            $view->setVariable('items', $search->getContent());
        } else {
            $view->setVariable('items', []);
        }
        return $view;
    }

    /**
     * 
     */
    public function memoryAction()
    {
        $items = $this->api()->search('items', ['item_set_id' => [$this->params('id')]])->getContent();
        $items = array_merge($items, $this->api()->search('items', ['item_set_id' => [$this->params('id')]])->getContent());

        shuffle($items);

        $view = new ViewModel;
        $view->setVariable('items', $items);
        return $view;
    }
}

<?php

namespace ParticipatoryArchives\Controller\Site;

use Error;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class CallsController extends AbstractActionController
{

    /**
     */
    public function indexAction()
    {
    }

    /**
     */
    public function createAction()
    {
    }

    /**
     */
    public function storeAction()
    {
        $collection_id = $this->params()->fromPost('collection_id');

        $rt_id_call = $this->settings()->get('rt_id_call');

        $data = [
            'o:resource_class' => [
                '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/25',
                'o:id' => 25
            ],
            'o:resource_template' => [
                '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_templates/' . $rt_id_call,
                'o:id' => $rt_id_call
            ],
            'schema:name' => [
                [
                    'type' => 'literal',
                    'property_id' => 957,
                    '@value' => $this->params()->fromPost('name')
                ]
            ],
            'dcterms:type' => [
                [
                    'type' => 'numeric:integer',
                    'property_id' => 8,
                    '@value' => $this->params()->fromPost('type')
                ]
            ],
            'o:item_set' => [
                [
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/item_sets/' . $collection_id,
                    'o:id' => $collection_id
                ]
            ]
        ];

        if ($this->params()->fromPost('description') != '') {
            $data['schema:description'] = [
                [
                    'type' => 'html',
                    'property_id' => 1621,
                    '@value' => $this->params()->fromPost('description')
                ]
            ];
        }

        if ($this->params()->fromPost('questions') != '') {
            $questions = [];

            $questions_array = preg_split("/\r\n|\n|\r/", $this->params()->fromPost('questions'));

            foreach ($questions_array as $key => $question) {
                $questions[] = [
                    'type' => 'literal',
                    'property_id' => 1151,
                    '@value' => $question
                ];
            }

            $data['schema:question'] = $questions;
        }

        if ($this->params()->fromPost('start_date') != '') {
            print($this->params()->fromPost('start_date'));

            $type = 'numeric:timestamp';
            $dates = [$this->params()->fromPost('start_date')];

            if ($this->params()->fromPost('end_date')) {
                $type = 'numeric:interval';
                $dates[] = $this->params()->fromPost('end_date');
            }

            $data['schema:temporal'] = [
                [
                    'type' => $type,
                    'property_id' => 1363,
                    '@value' => join('/', $dates)
                ]
            ];
        }

        if ($this->params()->fromPost('location_id') != '') {

            $location = $this->api()->read('items', $this->params()->fromPost('location_id'))->getContent();
            print($this->params()->fromPost('location_id') . '<br>');
            $data['schema:isRelatedTo'] = [
                [
                    'type' => 'resource:item',
                    'property_id' => 814,
                    '@id' => 'https://' . $_SERVER['HTTP_HOST'] . '/api/resource_classes/' . $location->id(),
                    'value_resource_id' => $location->id()
                ]
            ];
            if ($location->value('schema:geo') != '') {
                $data['schema:geo'] = [
                    [
                        'type' => 'geometry:geography:coordinates',
                        'property_id' => 681,
                        '@value' => $location->value('schema:geo')
                    ]
                ];
            }
        }

        if ($this->params()->fromPost('coordinates') != '') {
            $data['schema:geo'] = [
                [
                    'type' => 'geometry:geography:coordinates',
                    'property_id' => 681,
                    '@value' => $this->params()->fromPost('coordinates')
                ]
            ];
        }

        if ($this->params()->fromPost('keywords') != '') {
            $data['schema:keywords'] = [
                [
                    'type' => 'literal',
                    'property_id' => 844,
                    '@value' => $this->params()->fromPost('keywords')
                ]
            ];
        }

        $call = $this->api()->create('items', $data)->getContent();

        return $this->redirect()->toUrl('show/' . $call->id());
    }

    /**
     */
    public function showAction()
    {
        $response = $this->api()->read('items', $this->params('id'));
        $item = $response->getContent();

        $view = new ViewModel;
        $view->setVariable('item', $item);
        $view->setVariable('resource', $item);

        return $view;
    }

    /**
     */
    public function exportCsvAction()
    {
        $view = new ViewModel;
        $call = $this->api()->read('items', $this->params('id'))->getContent();

        foreach ($call->itemSets() as $key => $item_set) {
            $collection_id = $item_set->id();
        }

        $items = $this->api()
            ->search('items', [
                'item_set_id' => $collection_id,
            ])->getContent();

        

        $view->setVariable('filename', $call->displayTitle());
        $view->setVariable('items', $items);
        $view->setTerminal(true);
        return $view;
    }

    /**
     */
    public function exportMediaAction()
    {
        $view = new ViewModel;
        $call = $this->api()->read('items', $this->params('id'))->getContent();

        foreach ($call->itemSets() as $key => $item_set) {
            $collection_id = $item_set->id();
        }

        $items = $this->api()
            ->search('items', [
                'item_set_id' => $collection_id,
            ])->getContent();

        $view->setVariable('filename', $call->displayTitle());
        $view->setVariable('items', $items);
        $view->setTerminal(true);
        return $view;
    }
}

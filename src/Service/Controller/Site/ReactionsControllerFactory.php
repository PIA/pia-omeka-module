<?php
namespace ParticipatoryArchives\Service\Controller\Site;

use ParticipatoryArchives\Controller\Site\ReactionsController;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class ReactionsControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        return new ReactionsController($services);
    }
}

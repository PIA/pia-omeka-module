<?php declare(strict_types=1);

namespace ParticipatoryArchives\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class ConfigForm extends Form
{
    protected $globalSettings;

    public function init(): void
    {
        $this
            ->add([
                'name' => 'auto_add_new_collections_to_default_site',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Add new collections to default site',
                    'info' => 'Defines if user generated collections are automatically added to the ressources of the default site.',
                ],
            ])
            ->add([
                'name' => 'anonymous_key_identity',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'anonymous_key_identity',
                    'label' => 'Anonymous key identity string',
                ],
                'attributes' => [
                    // 'value' => $this->globalSettings->get('anonymous_key_identity'),
                ]
            ])
            ->add([
                'name' => 'anonymous_key_credential',
                'type' => Element\Text::class,
                'options' => [
                    'id' => 'anonymous_key_credential',
                    'label' => 'Anonymous key credential string',
                ],
                'attributes' => [
                    // 'value' => $this->globalSettings->get('anonymous_key_credential'),
                ]
            ])
        ;
    }

    public function setGlobalSettings($globalSettings)
    {
        $this->globalSettings = $globalSettings;
    }
}
